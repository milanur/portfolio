$(".slide-carousel").slick({
  dots: true,
});

$(".post-list").slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
});
