(function ($) {
  var slide = function (ele, options) {
    var $ele = $(ele);

    var setting = {
      speed: 500,
      interval: 2000,
    };

    $.extend(true, setting, options);

    var states = [
      {
        $zIndex: 3,
        width: 500,
        height: 400,
        top: -100,
        left: 134,
      },
      {
        $zIndex: 2,
        width: 500,
        height: 400,
        top: -80,
        left: -100,
      },
      {
        $zIndex: 1,
        width: 500,
        height: 400,
        top: -80,
        right: 10,
      },
    ];
    var $lis = $ele.find("li");

    $ele.find(".hi-next").on("click", function () {
      next();
    });
    $ele.find(".hi-prev").on("click", function () {
      states.push(states.shift());
      move();
    });
    move();

    function move() {
      $lis.each(function (index, element) {
        var state = states[index];
        $(element)
          .css("zIndex", state.$zIndex)
          .finish()
          .animate(state, setting.speed)
          .find("img");
      });
    }

    function next() {
      states.unshift(states.pop());
      move();
    }
  };

  $.fn.hiSlide = function (options) {
    $(this).each(function (index, ele) {
      slide(ele, options);
    });

    return this;
  };
})(jQuery);
