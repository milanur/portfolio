const navLinks = document.querySelector(".navigation-list");
const items = document.querySelectorAll(".latest-project-item");
const loadMoreButton = document.getElementById("load-more");
const loadLessButton = document.getElementById("load-less");
const btnAll = document.getElementById("btn-all");
const btnWeb = document.getElementById("btn-wd");
const btnMobile = document.getElementById("btn-ma");
const btnIlu = document.getElementById("btn-illu");
const btnPhoto = document.getElementById("btn-photo");
const boxes = document.querySelectorAll(".latest-project-item");
const menu = document.querySelector(".fas.fa-bars");
const closeMenu = document.querySelector(".fas.fa-times");
let itemCounter = 0;

showItems();

btnAll.addEventListener("click", () => {
  boxes.forEach((box) => {
    box.classList.remove("hide");
  });
});

btnWeb.addEventListener("click", () => {
  boxes.forEach((box) => {
    if (box.classList.contains("web-design")) {
      box.classList.remove("hide");
    } else {
      box.classList.add("hide");
    }
  });
});

btnIlu.addEventListener("click", () => {
  boxes.forEach((box) => {
    if (box.classList.contains("illustration")) {
      box.classList.remove("hide");
    } else {
      box.classList.add("hide");
    }
  });
});

btnMobile.addEventListener("click", () => {
  boxes.forEach((box) => {
    if (box.classList.contains("mobile-app")) {
      box.classList.remove("hide");
    } else {
      box.classList.add("hide");
    }
  });
});

btnPhoto.addEventListener("click", () => {
  boxes.forEach((box) => {
    if (box.classList.contains("photography")) {
      box.classList.remove("hide");
    } else {
      box.classList.add("hide");
    }
  });
});

loadMoreButton.addEventListener("click", showItems);
loadLessButton.addEventListener("click", hideItems);

function showItems() {
  for (let i = itemCounter; i < itemCounter + 6; i++) {
    if (items[i]) {
      items[i].classList.add("show");
    }
  }
  itemCounter += 3;

  if (itemCounter >= items.length) {
    loadMoreButton.disabled = true;
  }

  if (itemCounter < 3) {
    loadLessButton.disabled = false;
  }
}

function hideItems() {
  if (itemCounter > 3) {
    itemCounter -= 3;
    for (let i = itemCounter; i < itemCounter + 3; i++) {
      if (items[i]) {
        items[i].classList.remove("show");
      }
    }
  }

  if (itemCounter <= 0) {
    loadLessButton.disabled = true;
  }

  if (itemCounter < items.length - 3) {
    loadMoreButton.disabled = false;
  }
}

function showMenu() {
  navLinks.style.right = "0";
  menu.style.display = "none";
}

function hideMenu() {
  navLinks.style.right = "-200px";
  menu.style.display = "block";
  closeMenu.style.display = "none";
}

const videoPlayar = document.querySelector(".video-player");
const videoIcon = document.querySelector(".video-icon");
const videoClose = document.querySelector(".btn-close");

videoIcon.addEventListener("click", () => {
  videoPlayar.style.display = "block";
});

videoClose.addEventListener("click", () => {
  videoPlayar.style.display = "none";
});
